<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <UiMod name="BuffTrackerTweaks" version="1.0" date="1/1/2018">
        <VersionSettings gameVersion="1.4.8" windowsVersion="1.0" savedVariablesVersion="1.0"/>

        <Author name="cupnoodles" />
        <Description text="More buffs in one row, tighter rows"/>

        <Dependencies>
            <!-- <Dependency name="LibSlash"/> -->
            <!-- <Dependency name="EA_ThreePartBar"/> -->
            <Dependency name="EATemplate_UnitFrames"/>
            <!-- <Dependency name="EASystem_LayoutEditor"/> -->
            
        </Dependencies>

        <Files>
            <File name="bufftrackertweaks.lua" />
        </Files>
        
        <SavedVariables>
            <!-- <SavedVariable name="Addon.Settings" /> -->
        </SavedVariables>

        <OnInitialize>
            <!-- <CreateWindow name="Addon" show="true" /> -->
            <CallFunction name="BuffTrackerTweaks.Initialize" />
        </OnInitialize>

        <OnUpdate>
            <!-- <CallFunction name="BuffTrackerTweaks.Update" /> -->
        </OnUpdate>

        <OnShutdown/>
        
        <WARInfo>
            <Categories>
                <!-- <Category name="CRAFTING" /> -->
            </Categories>
        </WARInfo>

    </UiMod>
</ModuleFile>
