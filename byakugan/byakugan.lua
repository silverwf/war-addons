-- The MIT License (MIT)
--
-- Copyright (c) 2019 cupnoodles
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
-- SOFTWARE.
--
--
Byakugan = {}
ByakuganMarker = Frame:Subclass("ByakuganWindow")

Byakugan.Settings = {}

-- Byakugan.Settings.MarkFriendlyPlayers = true
-- Byakugan.Settings.MarkHostilePlayers = true
-- Byakugan.Settings.MarkHostileNpcs = true
-- Byakugan.Settings.MarkFriendlyNpcs = true
-- Byakugan.Settings.MarkOnTarget = true
-- Byakugan.Settings.MarkOnMouseover = true
-- Byakugan.Settings.MarkOnDamage = true
-- Byakugan.Settings.MarkWarband = false

Byakugan.Settings.FriendlyMarkers = 24
Byakugan.Settings.HostileMarkers = 24

Byakugan.Settings.CareerColors = {
    [20180] = {r = 175, g = 206, b = 234}, -- | Archmage
    [20181] = {r = 126, g = 57, b = 96}, -- | Blackguard
    [20182] = {r = 51, g = 115, b = 21}, -- | Black Orc
    [20183] = {r = 255, g = 37, b = 10}, -- | Bright Wizard
    [20184] = {r = 147, g = 200, b = 120}, -- | Choppa
    [20185] = {r = 99, g = 109, b = 112}, -- | Chosen
    [20186] = {r = 204, g = 43, b = 211}, -- | Disciple of Khaine
    [20187] = {r = 232, g = 181, b = 128}, -- | Engineer
    [20188] = {r = 179, g = 1, b = 1}, -- | Slayer
    [20189] = {r = 155, g = 82, b = 65}, -- | Ironbreaker
    [20190] = {r = 246, g = 106, b = 19}, -- | Knight of the Blazing Sun
    [20191] = {r = 44, g = 81, b = 136}, -- | Magus
    [20192] = {r = 177, g = 136, b = 134}, -- | Marauder
    [20193] = {r = 246, g = 227, b = 148}, -- | Rune Priest
    [20194] = {r = 144, g = 142, b = 165}, -- | Shadow Warrior
    [20195] = {r = 147, g = 200, b = 120}, -- | Shaman
    [20196] = {r = 92, g = 24, b = 125}, -- | Sorcerer/Sorceress
    [20197] = {r = 60, g = 96, b = 48}, -- | Squig Herder
    [20198] = {r = 52, g = 96, b = 159}, -- | Swordmaster
    [20199] = {r = 237, g = 129, b = 100}, -- | Warrior Priest
    [20200] = {r = 227, g = 240, b = 246}, -- | White Lion
    [20201] = {r = 188, g = 106, b = 204}, -- | Witch Elf
    [20202] = {r = 150, g = 26, b = 18}, -- | Witch Hunter
    [20203] = {r = 144, g = 142, b = 145} -- | Zealot
}

-- local HOSTILE_LOCKED = false
-- local FRIENDLY_LOCKED = false

local NUM_HOSTILE_MARKERS = Byakugan.Settings.HostileMarkers
local NUM_FRIENDLY_MARKERS = Byakugan.Settings.FriendlyMarkers

Byakugan.HostileMarkers = {}
Byakugan.FriendlyMarkers = {}
local HOSTILE_MARKERS = Byakugan.HostileMarkers
local FRIENDLY_MARKERS = Byakugan.FriendlyMarkers

local HOSTILE_MARKERS_BY_OBJ = {}
local FRIENDLY_MARKERS_BY_OBJ = {}

local TYPE_FRIENDLY = 0
local TYPE_HOSTILE = 1

local t = 0

-- temp trivial implementation -- TODO:
local i_friendly = 0
local i_hostile = 0

function Byakugan.Initialize()
    RegisterEventHandler(SystemData.Events.PLAYER_TARGET_UPDATED,
                         "Byakugan.OnTargetsUpdate")
    Byakugan.SetMarkersCounts(NUM_HOSTILE_MARKERS, NUM_FRIENDLY_MARKERS)
end

function Byakugan.RequestNextMarker(type, targetId)
    if type == TYPE_HOSTILE then
        i_hostile = (i_hostile + 1) % NUM_HOSTILE_MARKERS
        HOSTILE_MARKERS_BY_OBJ[targetId] = i_hostile + 1
        return HOSTILE_MARKERS[i_hostile + 1]
    else
        i_friendly = (i_friendly + 1) % NUM_FRIENDLY_MARKERS
        FRIENDLY_MARKERS_BY_OBJ[targetId] = i_friendly + 1
        return FRIENDLY_MARKERS[i_friendly + 1]
    end
end

function Byakugan.ClearOldMarkers(markers)
    for i, v in ipairs(markers) do if v:IsShowing() then v:HideIfOld() end end
end

function Byakugan.OnTargetsUpdate(targetClassification, targetId, targetType)
    TargetInfo:UpdateFromClient()
    local target = TargetInfo.m_Units[targetClassification]

    if FRIENDLY_MARKERS_BY_OBJ[targetId] ~= nil then
        if (targetId > 0 and targetId ~= GameData.Player.worldObjNum) then
            FRIENDLY_MARKERS[FRIENDLY_MARKERS_BY_OBJ[targetId]]:UpdateTarget(
                target)
        end
    elseif HOSTILE_MARKERS_BY_OBJ[targetId] ~= nil then
        if (targetId > 0 and targetId ~= GameData.Player.worldObjNum) then
            HOSTILE_MARKERS[HOSTILE_MARKERS_BY_OBJ[targetId]]:UpdateTarget(
                target)
        end
    else
        local type
        if TargetInfo:UnitIsFriendly(targetClassification) then
            type = TYPE_FRIENDLY
        else
            type = TYPE_HOSTILE
        end
        local marker = Byakugan.RequestNextMarker(type, targetId)
        marker:SetTarget(targetId, target)
    end
end

function Byakugan.SetMarkersCounts(num_friendly, num_hostile)
    NUM_FRIENDLY_MARKERS = num_friendly
    NUM_HOSTILE_MARKERS = num_hostile
    Byakugan.SetMarkersCount(FRIENDLY_MARKERS, num_friendly, "F")
    Byakugan.SetMarkersCount(HOSTILE_MARKERS, num_hostile, "H")
end

function Byakugan.SetMarkersCount(markers, count, wnamepart)
    local count_difference = count - #markers
    if count_difference > 0 then
        for i = 1, count_difference do
            local marker = ByakuganMarker:Create(
                               "ByakuganMarker" .. wnamepart ..
                                   tostring(#markers + 1))
            markers[#markers + 1] = marker
        end
    elseif count_difference < 0 then
        for i = 1, -count_difference do
            markers[#markers]:Destroy()
            markers[#markers] = nil
        end
    end
end

function Byakugan.CleanJunk()
    Byakugan.ClearOldMarkers(HOSTILE_MARKERS)
    Byakugan.ClearOldMarkers(FRIENDLY_MARKERS)
end

function Byakugan.OnUpdate(delta)
    t = t + delta
    if t > 3 then
        t = t - 3
        Byakugan.CleanJunk()
    end
end

--
-- marker
--

function ByakuganMarker:Create(windowName, parentWindow)
    local frame = self:CreateFromTemplate(windowName, parentWindow)

    if (frame) then
        frame.currentObjNum = nil
        frame:Show(false)
        frame:SetScale(0.6)
    end

    return frame
end

function ByakuganMarker:UpdateTarget(target)
    local careerline = tostring(target.level) -- title will contain level and NPC title, e.g. 40(40) Lord Butcher
    local nameline = target.name -- career will contain Name and career icon
    local careercolor = {r = 255, g = 255, b = 255}
    local healthPercent = target.healthPercent
    -- local objNum = TargetInfo:UnitEntityId(targetClassification)

    if target.battleLevel ~= target.level then
        careerline = careerline .. "(" .. tostring(target.battleLevel) .. ")"
    end
    careerline = towstring(careerline)

    if target.tier > 0 then
        local tiername
        if target.tier == 3 then
            tiername = L"L"
        elseif target.tier == 2 then
            tiername = L"H"
        elseif target.tier == 1 then
            tiername = L"C"
        end
        careerline = careerline .. tiername -- tiername .. L" " .. 
    end

    if target.career > 0 then
        local careerIcon = Icons.GetCareerIconIDFromCareerLine(target.career)
        nameline = nameline .. L" <icon" .. towstring(careerIcon) .. L">"
        careerline = target.careerName .. L" " .. careerline
        local ccolor = Byakugan.Settings.CareerColors[careerIcon]
        careercolor = ccolor
    else
        careerline = towstring(target.npcTitle) .. L" " .. careerline
    end

    local windowname = self:GetName()
    LabelSetText(windowname .. "Name", nameline)
    LabelSetTextColor(windowname .. "Name", target.relationshipColor.r,
                      target.relationshipColor.g, target.relationshipColor.b)

    LabelSetText(windowname .. "Career", careerline)
    LabelSetTextColor(windowname .. "Career", careercolor.r, careercolor.g,
                      careercolor.b)
    -- WindowSetAlpha(windowname .. "Name", 0.5)
    -- WindowSetAlpha(windowname .. "Career", 0.5)
    -- self:SetAlpha(0.1)
end

function ByakuganMarker:HideIfOld()
    local wname = self:GetName()
    if self.currentObjNum > 0 then
        DetachWindowFromWorldObject(wname, self.currentObjNum)
        WindowClearAnchors(wname)
        local x_old, y_old = WindowGetScreenPosition(wname)
        local dif = 100
        WindowAddAnchor(wname, "center", "Root", "center", x_old + dif,
                        y_old + dif)

        MoveWindowToWorldObject(wname, self.currentObjNum, 1)
        local x, y = WindowGetScreenPosition(wname)
        if x ~= x_old + dif or y ~= y_old + dif then
            WindowClearAnchors(wname)
            AttachWindowToWorldObject(wname, self.currentObjNum)
            MoveWindowToWorldObject(wname, self.currentObjNum, 1)
        else
            self:Show(false)
        end
    end
end

function ByakuganMarker:SetTarget(objNum, target)
    local wname = self:GetName()
    d("setting target")

    if ((objNum > 0) and (objNum ~= GameData.Player.worldObjNum) and
        (objNum ~= self.currentObjNum)) then
        d("attach marker" .. wname, target.name, target.entityId)
        if (self.currentObjNum) then
            DetachWindowFromWorldObject(wname, self.currentObjNum)
            FRIENDLY_MARKERS_BY_OBJ[self.currentObjNum] = nil
            HOSTILE_MARKERS_BY_OBJ[self.currentObjNum] = nil
        end

        self.currentObjNum = objNum
        WindowSetShowing(wname, true)
        self:UpdateTarget(target)
        AttachWindowToWorldObject(wname, objNum)
        MoveWindowToWorldObject(wname, objNum, 1)
        self:Show(true)
    elseif (self.currentObjNum and not objNum) then
        d("detach marker" .. wname)
        DetachWindowFromWorldObject(wname, self.currentObjNum)
        WindowSetShowing(wname, false)
        FRIENDLY_MARKERS_BY_OBJ[self.currentObjNum] = nil
        HOSTILE_MARKERS_BY_OBJ[self.currentObjNum] = nil
        self:Show(false)
        self.currentObjNum = objNum
    end
end

