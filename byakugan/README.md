# Byakugan

Automatic target marking on mouseover.

You were already relying on Enemy marks, TargetRing and these floating BuffHead effects to track enemies behind obstacles and/or out of target range, right?

## TODO:

- [x] Basic implementation
- [ ] Toggle automatic marking
  - [ ] a setting to mark next N players etc.
- [ ] Disable marking by:
  - [ ] entity type - player/npc
  - [ ] event - on damage/mouseover/heal
  - [ ] entity relationship - friendly/hostile
- [ ] Limit marks count by entity type/relationship etc
- [ ] Better old marks cleanup algorithm
- [ ] Target locking
  - [ ] Lock target by name
  - [ ] Lock N last targets
  - [ ] Lock indication on the mark
  - [ ] Locked target cleanup notification
  - [ ] Persistent lock list
- [ ] Basic LibUiButton interface and a macro to show the menu
