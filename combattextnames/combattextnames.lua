-- The MIT License (MIT)
--
-- Copyright (c) 2019 cupnoodles
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
-- SOFTWARE.
--
--
CombatTextNames = {}

-- these can be changed with /script and are reset on restarts
-- although you could set initial values here
CombatTextNames.CombatTextEnabled = true
CombatTextNames.IncomingHealsEnabled = true
CombatTextNames.IncomingMissesEnabled = true
CombatTextNames.IncomingDamageEnabled = true

-- constants, do not toucha!
local COMBAT_EVENT = 1

function CombatTextNames.Initialize()
    -- hooks
    EA_System_PointGainEntry.SetupText =
        CombatTextNames.HookPointGainTextScaler(
            EA_System_PointGainEntry.SetupText)

    if CombatTextNames.Settings.ResetPositionOnLOS then
        EA_System_EventTracker.Update = CombatTextNames.HookLOSPositioner(
                                            EA_System_EventTracker.Update)
    end

    -- REPLACEMENTS
    EA_System_EventText.AddCombatEventText = CombatTextNames.AddCombatEventText
    EA_System_EventEntry.SetupText = CombatTextNames.SetupText
    EA_System_EventTracker.InitializeAnimationData =
        CombatTextNames.InitializeAnimationData

    EA_System_EventEntry.m_Template = "CombatTextNames_Window_EventTextLabel"
    EA_System_PointGainEntry.m_Template =
        "CombatTextNames_Window_EventTextLabel"
end

function CombatTextNames.HookPointGainTextScaler(fn)
    return function(self, hitTargetObjectNumber, pointAmount, pointType)
        fn(self, hitTargetObjectNumber, pointAmount, pointType)
        self:SetRelativeScale(CombatTextNames.Settings.PointGainScaleFactor)
    end
end

-- CombatTextNames.AddCombatEventText is a replacement
-- for EA_System_EventText.AddCombatEventText which adds additional fields
-- (skill name and icon) to events it creates.
function CombatTextNames.AddCombatEventText(hitTargetObjectNumber, hitAmount,
                                            textType, abilityID)
    -- skip incoming events if they're toggled off
    if (hitTargetObjectNumber == GameData.Player.worldObjNum) and
        ((hitAmount < 0 and not CombatTextNames.IncomingDamageEnabled) or
            (hitAmount > 0 and not CombatTextNames.IncomingHealsEnabled) or
            (hitAmount == 0 and not CombatTextNames.IncomingMissesEnabled)) then
        return
    end

    local data = GetAbilityData(abilityID)

    -- The SetupText function is going to receive only eventData.amount member.
    -- So we replace eventData.amount with a table that contains all the data we
    -- need. That is, we add ability name there. Doing things the "proper" way
    -- would require replacing more EA code, I'd rather avoid this.
    local eventData = {
        event = COMBAT_EVENT,
        amount = {
            hit = hitAmount,
            name = data.name,
            abilityID = abilityID,
            iconNum = data.iconNum
        },
        type = textType
    }

    if (EA_System_EventText.EventTrackers[hitTargetObjectNumber] == nil) then
        local newTrackerAnchorWindowName =
            "EA_System_EventTextAnchor" .. hitTargetObjectNumber
        CreateWindowFromTemplate(newTrackerAnchorWindowName,
                                 "EA_Window_EventTextAnchor",
                                 "EA_Window_EventTextContainer")
        EA_System_EventText.EventTrackers[hitTargetObjectNumber] =
            EA_System_EventTracker:Create(newTrackerAnchorWindowName,
                                          hitTargetObjectNumber)
        -- For CombatTextNames.HookLOSPositioner, mark self as attached:
        EA_System_EventText.EventTrackers[hitTargetObjectNumber]
            .attachedToObject = true
    end

    EA_System_EventText.EventTrackers[hitTargetObjectNumber]:AddEvent(eventData)
end

-- CombatTextNames.SetupText is a replacement for EA_System_EventEntry.SetupText
-- that expects a `hitData` table (containing hit amount, skill name and icon)
-- instead of `amount` as the 3rd argument, and uses the bonus fields to set up
-- the combat text with an icon and/or a skill name.
function CombatTextNames.SetupText(self, hitTargetObjectNumber, hitData,
                                   textType)
    if CombatTextNames.Settings.EnableIcons and hitData.iconNum ~= 0 then
        local texture, x, y = GetIconData(hitData.iconNum)
        DynamicImageSetTexture(self:GetName() .. "Icon", texture, x, y)
    end

    local name = L""
    if CombatTextNames.Settings.EnableAbilityNames and hitData.name ~= L"" then
        -- here's code to add an icon as a text icon (but it won't be animated correctly):
        -- name = L"<icon" .. towstring(hitAmount.iconNum) .. L"> " .. name
        -- LabelSetIgnoreFormattingTags(self:GetName(), false)
        name = L" (" .. towstring(hitData.name) .. L")"
    end

    local amount = hitData.hit

    local color = CombatTextNames.GetCombatEventColor(hitTargetObjectNumber,
                                                      amount, textType)
    LabelSetTextColor(self:GetName(), color.r, color.g, color.b)

    local text = L""
    local scaling = CombatTextNames.Settings.OwnEventScaleFactor

    if ((textType == GameData.CombatEvent.HIT) or
        (textType == GameData.CombatEvent.ABILITY_HIT) or
        (textType == GameData.CombatEvent.CRITICAL) or
        (textType == GameData.CombatEvent.ABILITY_CRITICAL)) then
        if (amount > 0) then
            text = L"+" .. amount .. name
        else
            text = L"" .. amount .. name
        end

        if ((textType == GameData.CombatEvent.CRITICAL) or
            (textType == GameData.CombatEvent.ABILITY_CRITICAL)) then
            text = text .. CombatTextNames.Settings.CritPostfix
            scaling = CombatTextNames.Settings.CriticalScaleFactor
        end
    else
        text = CombatTextNames.EventAdditionalLabel[textType]
        if CombatTextNames.Settings.EnableNamesOnMissText then
            text = text .. name
        end
    end

    self:SetRelativeScale(scaling)
    LabelSetText(self:GetName(), text)
end

-- CombatTextNames.HookLOSPositioner will try to position EventTracker
-- on screen according to LOS: in the center of the screen if the worldobj is
-- out of LOS, or attached to worldobj otherwise.
-- It is meant to be hooked on EA_System_EventTracker.Update
function CombatTextNames.HookLOSPositioner(fn)
    -- Note: throttling like this is incorrect as it results in having a
    -- single common timer for multiple objects. But in practice it works out
    -- okay and keeps the code a bit simpler.
    local t = 0
    return function(self, elapsedTime)
        t = t + elapsedTime
        if t >= CombatTextNames.Settings.LOSCheckPeriod and self.m_TargetObject then
            t = t - CombatTextNames.Settings.LOSCheckPeriod
            if not self.attachedToObject then
                -- NOTE: atm AttachWindowToWorldObject doesn't support heights
                -- different than `1` so using self.m_AttachHeight would
                -- result in flicker.
                MoveWindowToWorldObject(self.m_Anchor, self.m_TargetObject, 1)
            end

            if not WindowGetShowing(self.m_Anchor) then
                self.attachedToObject = false
                DetachWindowFromWorldObject(self.m_Anchor, self.m_TargetObject)
                WindowSetShowing(self.m_Anchor, true)
                WindowClearAnchors(self.m_Anchor)
                WindowAddAnchor(self.m_Anchor, "center", "Root", "center", 0, 0)
            elseif not self.attachedToObject then
                self.attachedToObject = true
                WindowClearAnchors(self.m_Anchor)
                DetachWindowFromWorldObject(self.m_Anchor, self.m_TargetObject)
                AttachWindowToWorldObject(self.m_Anchor, self.m_TargetObject)
            end
        end

        fn(self, elapsedTime)
    end
end

-- CombatTextNames.InitializeAnimationData is a replacement
-- for EA_System_EventTracker:InitializeAnimationData
function CombatTextNames.InitializeAnimationData(self, displayType)
    local baseAnimation = CombatTextNames.Settings.Animation.Friendly

    if (displayType == COMBAT_EVENT) then
        if (self.m_TargetObject == GameData.Player.worldObjNum) then
            baseAnimation = CombatTextNames.Settings.Animation.Friendly
        else
            baseAnimation = CombatTextNames.Settings.Animation.Hostile
        end
    else
        baseAnimation = CombatTextNames.Settings.Animation.Default
    end

    local animationData = {
        start = {x = baseAnimation.start.x, y = baseAnimation.start.y},
        target = {x = baseAnimation.target.x, y = baseAnimation.target.y},
        current = {x = baseAnimation.start.x, y = baseAnimation.start.y},
        maximumDisplayTime = baseAnimation.maximumDisplayTime,
        fadeDelay = baseAnimation.fadeDelay,
        fadeDuration = baseAnimation.fadeDuration
    }

    return animationData
end

-- CombatTextNames.GetCombatEventColor is an alternative to DefaultColor.GetCombatEventColor
-- with configurable colors (although you could hijack DefaultColor...).
function CombatTextNames.GetCombatEventColor(hitTargetObjectNumber, hitAmount,
                                             textType)
    local colors = CombatTextNames.Settings.Colors
    local isPlayer = hitTargetObjectNumber == GameData.Player.worldObjNum

    if (hitAmount < 0) then
        local color = CombatTextNames.AttackColors[isPlayer][textType]
        if color then
            return color
        end

        return colors.INCOMING_DAMAGE
    end

    if (hitAmount > 0) then
        local color = CombatTextNames.DefenseColors[isPlayer][textType]
        if color then
            return color
        end

        if isPlayer then
            return colors.INCOMING_HEALING
        end

        return colors.OUTGOING_HEALING
    end

    if isPlayer then
        return colors.INCOMING_MISS
    end

    return colors.OUTGOING_MISS
end
