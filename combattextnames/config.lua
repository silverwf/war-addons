CombatTextNames.Settings = {
    ResetPositionOnLOS = true,
    LOSCheckPeriod = 0.5,
    EnableIcons = true,
    EnableAbilityNames = false,
    EnableNamesOnMissText = true, -- only works if names are enabled
    CritPostfix = L"",
    Colors = {
        INCOMING_DAMAGE = {r = 255, g = 0, b = 0},
        INCOMING_DAMAGE_CRIT = {r = 255, g = 0, b = 0},
        OUTGOING_DAMAGE = {r = 235, g = 235, b = 235},
        OUTGOING_DAMAGE_CRIT = {r = 235, g = 235, b = 235},
        INCOMING_SPECIAL_DAMAGE = {r = 255, g = 66, b = 0},
        INCOMING_SPECIAL_DAMAGE_CRIT = {r = 255, g = 66, b = 0},
        OUTGOING_SPECIAL_DAMAGE = {r = 235, g = 215, b = 135},
        OUTGOING_SPECIAL_DAMAGE_CRIT = {r = 235, g = 215, b = 135},
        INCOMING_ABSORB = {r = 11, g = 66, b = 233},
        OUTGOING_ABSORB = {r = 11, g = 66, b = 233},
        INCOMING_HEALING = {r = 0, g = 200, b = 0},
        OUTGOING_HEALING = {r = 0, g = 138, b = 0},
        INCOMING_MISS = {r = 228, g = 228, b = 228},
        OUTGOING_MISS = {r = 156, g = 156, b = 156},
        EXPERIENCE_GAIN = {r = 255, g = 170, b = 0},
        RENOWN_GAIN = {r = 194, g = 56, b = 153},
        INFLUENCE_GAIN = {r = 0, g = 170, b = 163}
    },
    PointGainScaleFactor = 1.3,
    CriticalScaleFactor = 1.3,
    OwnEventScaleFactor = 1,
    Animation = {
        Default = {
            start = {x = -280, y = 100},
            target = {x = -280, y = 20},
            current = {x = -280, y = 100},
            maximumDisplayTime = 4,
            fadeDelay = 2,
            fadeDuration = 0.75
        },
        Hostile = {
            start = {x = 0, y = -100},
            target = {x = 0, y = -180},
            current = {x = 0, y = -100},
            maximumDisplayTime = 4,
            fadeDelay = 2,
            fadeDuration = 0.75
        },
        Friendly = {
            start = {x = -200, y = -90},
            target = {x = -200, y = -170},
            current = {x = -200, y = -90},
            maximumDisplayTime = 4,
            fadeDelay = 2,
            fadeDuration = 0.75
        }
    }
}

-- coloring
-- [true] is for incoming events (isPlayer == true)
-- For saved settings to actually work, this has to be moved inside a function
-- otherwise it stores the default color value.
--
-- If you were to merge DefenseColors and AttackColors tables as they are now, heals
-- would become colored as attacks bc heals are GameData.CombatEvent.ABILITY_HIT)
-- TODO: may probably want to set is as Settings members.
local COLORS = CombatTextNames.Settings.Colors
CombatTextNames.DefenseColors = {
    -- is player
    [true] = {
        [GameData.CombatEvent.ABSORB] = COLORS.INCOMING_ABSORB,
        [GameData.CombatEvent.BLOCK] = COLORS.INCOMING_MISS,
        [GameData.CombatEvent.PARRY] = COLORS.INCOMING_MISS,
        [GameData.CombatEvent.DISRUPT] = COLORS.INCOMING_MISS,
        [GameData.CombatEvent.EVADE] = COLORS.INCOMING_MISS,
        [GameData.CombatEvent.IMMUNE] = COLORS.INCOMING_MISS,
        [GameData.CombatEvent.BLOCK] = COLORS.INCOMING_MISS
    },
    -- not player
    [false] = {
        [GameData.CombatEvent.ABSORB] = COLORS.OUTGOING_ABSORB,
        [GameData.CombatEvent.BLOCK] = COLORS.OUTGOING_MISS,
        [GameData.CombatEvent.PARRY] = COLORS.OUTGOING_MISS,
        [GameData.CombatEvent.DISRUPT] = COLORS.OUTGOING_MISS,
        [GameData.CombatEvent.EVADE] = COLORS.OUTGOING_MISS,
        [GameData.CombatEvent.IMMUNE] = COLORS.OUTGOING_MISS,
        [GameData.CombatEvent.BLOCK] = COLORS.OUTGOING_MISS
    }
}

CombatTextNames.AttackColors = {
    -- is player
    [true] = {
        [GameData.CombatEvent.HIT] = COLORS.INCOMING_DAMAGE,
        [GameData.CombatEvent.CRITICAL] = COLORS.INCOMING_DAMAGE_CRIT,
        [GameData.CombatEvent.ABILITY_HIT] = COLORS.INCOMING_SPECIAL_DAMAGE,
        [GameData.CombatEvent.ABILITY_CRITICAL] = COLORS.INCOMING_SPECIAL_DAMAGE_CRIT
    },
    -- not player
    [false] = {
        [GameData.CombatEvent.HIT] = COLORS.OUTGOING_DAMAGE,
        [GameData.CombatEvent.CRITICAL] = COLORS.OUTGOING_DAMAGE_CRIT,
        [GameData.CombatEvent.ABILITY_HIT] = COLORS.OUTGOING_SPECIAL_DAMAGE,
        [GameData.CombatEvent.ABILITY_CRITICAL] = COLORS.OUTGOING_SPECIAL_DAMAGE_CRIT
    }
}

CombatTextNames.EventAdditionalLabel = {
    [GameData.CombatEvent.HIT] = L"",
    [GameData.CombatEvent.ABILITY_HIT] = L"",
    [GameData.CombatEvent.CRITICAL] = L"",
    [GameData.CombatEvent.ABILITY_CRITICAL] = L"",
    [GameData.CombatEvent.BLOCK] = GetStringFromTable("CombatEvents",
                                                      StringTables.CombatEvents
                                                          .LABEL_BLOCK),
    [GameData.CombatEvent.PARRY] = GetStringFromTable("CombatEvents",
                                                      StringTables.CombatEvents
                                                          .LABEL_PARRY),
    [GameData.CombatEvent.EVADE] = GetStringFromTable("CombatEvents",
                                                      StringTables.CombatEvents
                                                          .LABEL_EVADE),
    [GameData.CombatEvent.DISRUPT] = GetStringFromTable("CombatEvents",
                                                        StringTables.CombatEvents
                                                            .LABEL_DISRUPT),
    [GameData.CombatEvent.ABSORB] = GetStringFromTable("CombatEvents",
                                                       StringTables.CombatEvents
                                                           .LABEL_ABSORB),
    [GameData.CombatEvent.IMMUNE] = GetStringFromTable("CombatEvents",
                                                       StringTables.CombatEvents
                                                           .LABEL_IMMUNE)
}
