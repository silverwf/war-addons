ShowMeTheBubbles = {}
ShowMeTheBubbles.Settings = {
    Version = "1.0.0",
    ShowSettingInChatMenu = true,
    Channels = {
        [SystemData.ChatLogFilters.BATTLEGROUP] = true,
        [SystemData.ChatLogFilters.SHOUT] = true,
        [SystemData.ChatLogFilters.TELL_RECEIVE] = true,
        [SystemData.ChatLogFilters.TELL_SEND] = true,
        [SystemData.ChatLogFilters.SCENARIO] = true,
        [SystemData.ChatLogFilters.SCENARIO_GROUPS] = true,
        [SystemData.ChatLogFilters.CHANNEL_1] = true,
        [SystemData.ChatLogFilters.CHANNEL_2] = true
    }
}

local DEFAULT_SETTINGS = ShowMeTheBubbles.Settings -- saves reference to Settings
-- Settings will be replaced with game's saved Settings before running Initialize()

local channelNames = {
    [SystemData.ChatLogFilters.BATTLEGROUP] = L"Warband",
    [SystemData.ChatLogFilters.SHOUT] = L"Shout",
    [SystemData.ChatLogFilters.TELL_RECEIVE] = L"Tell receive",
    [SystemData.ChatLogFilters.TELL_SEND] = L"Tell send",
    [SystemData.ChatLogFilters.SCENARIO] = L"Scenario",
    [SystemData.ChatLogFilters.SCENARIO_GROUPS] = L"Scenario party",
    [SystemData.ChatLogFilters.CHANNEL_1] = L"Region",
    [SystemData.ChatLogFilters.CHANNEL_2] = L"Region RVR"
}

function ShowMeTheBubbles.Initialize()
    ShowMeTheBubbles.chatMenuHooked = false

    if ShowMeTheBubbles.Settings then
        if ShowMeTheBubbles.Settings.Version ~= DEFAULT_SETTINGS.Version then
            ShowMeTheBubbles.Settings = DEFAULT_SETTINGS
        end
    end

    ShowMeTheBubbles.hookChatMenu(true)
    RegisterEventHandler(SystemData.Events.CHAT_TEXT_ARRIVED,
                         "ShowMeTheBubbles.OnChatText")
end

function ShowMeTheBubbles.OnChatText()
    if (ShowMeTheBubbles.Settings.Channels[GameData.ChatData.type]) then
        ChatManager.AddChatText(GameData.ChatData.objectId,
                                GameData.ChatData.text)
    end
end

function ShowMeTheBubbles.hookChatMenu(enable)
    if enable and not ShowMeTheBubbles.chatMenuHooked then
        ShowMeTheBubbles.OnOpenChatMenuHook = EA_ChatWindow.OnOpenChatMenu
        EA_ChatWindow.OnOpenChatMenu = ShowMeTheBubbles.OnOpenChatMenu
        ShowMeTheBubbles.chatMenuHooked = true

    elseif ShowMeTheBubbles.chatMenuHooked then
        EA_ChatWindow.OnOpenChatMenu = ShowMeTheBubbles.OnOpenChatMenuHook
        ShowMeTheBubbles.chatMenuHooked = false
    end
end

function ShowMeTheBubbles.OnOpenChatMenu(...)
    ShowMeTheBubbles.OnOpenChatMenuHook(...)
    local menuItemsAdded = false

    if (ShowMeTheBubbles.Settings.showSettingInChatMenu) then
        EA_Window_ContextMenu.AddCascadingMenuItem(L"Additional speech bubbles",
                                                   ShowMeTheBubbles.SpawnOptionsMenu,
                                                   false,
                                                   EA_Window_ContextMenu.CONTEXT_MENU_1)
        menuItemsAdded = true
    end

    if (menuItemsAdded) then
        EA_Window_ContextMenu.Finalize(EA_Window_ContextMenu.CONTEXT_MENU_1)
    end
end

function ShowMeTheBubbles.SpawnOptionsMenu()

    EA_Window_ContextMenu.Hide(EA_Window_ContextMenu.CONTEXT_MENU_3)
    EA_Window_ContextMenu.CreateContextMenu("",
                                            EA_Window_ContextMenu.CONTEXT_MENU_2)

    local function toggleText(state)
        if state then
            return L"[o] "
        else
            return L"[ ] "
        end
    end

    for k, v in pairs(channelNames) do
        EA_Window_ContextMenu.AddMenuItem(
            toggleText(ShowMeTheBubbles.Settings.Channels[k]) .. v, function()
                ShowMeTheBubbles.Settings.Channels[k] =
                    not ShowMeTheBubbles.Settings.Channels[k]
            end, false, true, EA_Window_ContextMenu.CONTEXT_MENU_2)
    end

    EA_Window_ContextMenu.Finalize(EA_Window_ContextMenu.CONTEXT_MENU_2)
end
