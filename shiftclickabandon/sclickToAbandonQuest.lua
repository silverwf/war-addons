--[[--------------------------------------------------------------------------
clickToAbandon

use shift-click on a quest entry in the quest journal to abandon the quest
--]]--------------------------------------------------------------------------

clickToAbandon = {}

-- CONFIG --------------------------------------------------------------------

-- if false, show quest text and objectives in confirmation window, 
-- else - abandon with no confirmation
local instant_abandon = false

------------------------------------------------------------------------------

-- locals
local original = nil
local pendingabandon = nil

function clickToAbandon.Initialize()
    original = TomeWindow.SelectQuest
    TomeWindow.SelectQuest = clickToAbandon.SelectQuest
end

function clickToAbandon.SelectQuest ( flags, x, y )
    local questId = WindowGetId( WindowGetParent( SystemData.ActiveWindow.name) )
    if flags == SystemData.ButtonFlags.SHIFT then
        if instant_abandon then
            AbandonQuest (questId)
        else
            pendingabandon = questId
            
            local questData = DataUtils.GetQuestData( questId )
            local text = questData.startDesc .. L"\n\n" .. questData.journalDesc
            DialogManager.MakeTwoButtonDialog( text, GetString( StringTables.Default.LABEL_ABANDON ), 
                clickToAbandon.DoAbandon, GetString( StringTables.Default.LABEL_CANCEL ), nil )
        end
    else
        local params = { questId }
        TomeWindow.SetState( TomeWindow.PAGE_QUEST_INFO, params )      
    end
end

function clickToAbandon.DoAbandon ()
    AbandonQuest (pendingabandon)
    pendingabandon = nil
end