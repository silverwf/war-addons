
TintUnsellable = {}

-- locals
local atStore = false
local EA_Window_Backpack_SetActionButton
local EA_Window_Backpack_OnShown

function TintUnsellable.Initialize()
    EA_Window_Backpack_SetActionButton = EA_Window_Backpack.SetActionButton
    EA_Window_Backpack.SetActionButton = TintUnsellable.SetActionButton

    EA_Window_Backpack_OnShown = EA_Window_Backpack.OnShown
    EA_Window_Backpack.OnShown = TintUnsellable.OnShown

    RegisterEventHandler(SystemData.Events.INTERACT_DONE, "TintUnsellable.UpdateSlots")
    RegisterEventHandler(SystemData.Events.INTERACT_SHOW_STORE, "TintUnsellable.UpdateSlots")
end

function TintUnsellable.OnShown()
    EA_Window_Backpack_OnShown()
    TintUnsellable.UpdateSlots()
end

function TintUnsellable.UpdateSlots()
    local b = EA_Window_InteractionStore.InteractingWithStore() or EA_Window_InteractionLibrarianStore.InteractingWithLibrarianStore()
    if b ~= atStore then
        -- refresh icons every time we come to or leave the vendor so that tint doesn't stay when not needed
        atStore = b
        EA_Window_Backpack.UpdateBackpackSlots()
    end
end

function TintUnsellable.SetActionButton( buttonGroupName, buttonIndex, itemData, isLocked, highLightColor )
    EA_Window_Backpack_SetActionButton(buttonGroupName, buttonIndex, itemData, isLocked, highLightColor)
    if itemData.id ~= 0 and itemData.type ~= GameData.ItemTypes.CURRENCY then
        local unsellable = itemData.flags[GameData.Item.EITEMFLAG_NO_SELL] or itemData.sellPrice <= 0
        if unsellable and atStore then
            ActionButtonGroupSetTintColor( buttonGroupName, buttonIndex, 125, 0, 0)
        elseif not atStore then
            ActionButtonGroupSetTintColor( buttonGroupName, buttonIndex, 255, 255, 255)
        end
    end
end