# My WAR Addons Collection

These are some small addons for Warhammer Online.

Many of them are simple patches to EA code and I'm not sure under which license they belong. My original code is under MIT (personally, I don't really care for attribution or anything).

## Contents

| Name              | Description                                                                                                                                                                                         |
| ----------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| BankWindowFix     | Fixes stacking of items when putting them in bank, fixes right-click not putting items into backpack.                                                                                               |
| BiggerMacroWindow | (EA macro window with edited .xml) Show all macros in the macro window at once.                                                                                                                     |
| BuffTrackerTweaks | Show more buffs on standard unitframes                                                                                                                                                              |
| Byakugan          | Automatic target marking on mouseover. You were already relying on Enemy marks, TargetRing and these floating BuffHead effects to track enemies behind obstacles and/or out of target range, right? |
| CastbarGCD        | Default castbar shows global cooldown when you cast an instant ability (like Effigy/Amethyst/Obsidian). Because instant abilities lack feedback in the big fights.                                  |
| CombatTextNames   | Show ability names on the default combat text (damage numbers), change font to bold sans serif. Smaller alternative to WSCT. Icons are buggy so they're not enabled.                                |
| FixStuckBuffs     | Fix stuck buffs on standard unitframes (happens when BuffHead is enabled).                                                                                                                          |
| FollowTheLeader   | A macro to follow the warband leader. Addon handles updating the leader name in the macro.                                                                                                          |
| HideHiddenFrames  | Hide hidden gray frames in UI editor. You can unhide them using a menu anyway.                                                                                                                      |
| LargeBackpackFont | Large bold font in the backpack (currently except quest bag but who uses it).                                                                                                                       |
| ShiftClickAbandon | Shift click on a quest in the Journal to quickly abandon it.                                                                                                                                        |
| ShowMeTheBubbles  | Chat bubbles for `/wb`, `/1`, `/2` etc. Lets you see that the wb leader said something or quickly identify the direction the calls for help are coming from etc.                                    |
| TargetInfoRing    | (Modification of Talvinen's TargetRing) TargetRing with HP/name/carreer. Because every other HUD addon (Pure, Effigy, RV) is bloated.                                                               |
| TintUnsellable    | Color unsellable items dark red when at vendor.                                                                                                                                                     |

Other repos:

| Name                                                          | Description                                                                                                                                             |
| ------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [NerfedNB](https://gitlab.com/cupnoodles14/nerfednb/)         | stripped down NerfedButtons. No conditionals - it only checks if ability is castable at all, and if not, it will cast the next ability in the sequence. |
| [GatherButton](https://gitlab.com/cupnoodles14/gatherbutton/) | cultivate without clicking the mouse.                                                                                                                   |

## Roadmap

I would like to replace the big addons (RV, Pure, Effigy, MGR, WSCT, maybe some day Enemy too) with smaller ones that have less overhead, while keeping the features that I deem essential.

- [x] Sane cultivation workflow (MGR)
  - [x] planting/gathering
  - [ ] nutrients/soil/water
- [x] Castbar (Obsidian/Amethyst)
  - [x] Global cooldown indication
- [x] Combat text (WSCT)
  - [x] Bold sans serif font
  - [x] Ability names
    - [ ] Abbreviations
  - [ ] Ability icons
- [x] HUD (Pure/Effigy/RV)
- [ ] Target unitframes (Pure/Effigy/RV)
  - [ ] More compact than the default GUI
  - [ ] Sane buff tracker
  - [ ] Dispel indication
  - [ ] HP percentages
  - [ ] AP bar
    - [ ] Also look into AP regen delay indication
- [ ] Warband unitframes (Enemy/Squared/RV)
  - [ ] Click casting
  - [ ] Effect indicators
- [ ] Target assist (compatible with Enemy messages)
- [ ] Targeting/following the guardee

## Misc ideas

- [ ] Nicer, more compact actionbars
- [ ] 'Auto'-assist
      (it's possible to set an action button to target an ally and it's possible to set an action button to send /assist command. since people are button-mashing anyway, this could be basically an autoassist)
- [ ] Performance modes (disable/reenable some non-essential addons' functionality and change user settings (e.g. player names rendering) etc. Either automatically, based on FPS, or with a hotkey. Basically, [this](https://www.returnofreckoning.com/forum/viewtopic.php?f=5&t=20075) but with a one-click configuration)
- [ ] Duplicate all on-screen announcements (forgot what it's called) in chat.
- [ ] Skill recommendation system instead of nerfedbuttons (reminder to meditate, cast runes/rituals/buffs/guards).
- [ ] ScenarioAlert demonstrates a cool way to communicate with the outer world, there should be some more insteresting uses.
  - It's possible to make an automated system of enemy location reports and an online map of players.
    - Doing this would be kinda bad for the gameplay because one could snitch on their own realm. If such a map were to implemented, only verified users shall be able to send location info, otherwise it'd be easy to spam the map with false info.
- [ ] Fix/reimplement bagomatic
