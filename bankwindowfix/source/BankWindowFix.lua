----------------------------------------------------------------
-- Global Variables
----------------------------------------------------------------

BankWindowFix = {}

local WINDOW_NAME = "BankWindow"
local SLOTS_NAME = WINDOW_NAME.."Slots"
local NUM_COLS = 8
local NUM_ROWS = 10


local ENABLE_CTRLRCLICK_DELETE = false

function BankWindowFix.Initialize()

    --bank hook and replace window with fixed version
    local orig_BankEquipmentRButtonDown = BankWindow.EquipmentRButtonDown
    BankWindow.EquipmentRButtonDown = BankWindowFix.BankEquipmentRButtonDown

    DestroyWindow(SLOTS_NAME)
    CreateWindowFromTemplate(SLOTS_NAME, "BankWindowSlotsFixed", WINDOW_NAME)
    ActionButtonGroupSetNumButtons( SLOTS_NAME, NUM_ROWS, NUM_COLS )

    --backpack hook so that items would try to stack when moving from bag to bank
    local orig_BackpackEquipmentRButtonUp = EA_Window_Backpack.EquipmentRButtonUp
    EA_Window_Backpack.EquipmentRButtonUp = BankWindowFix.BagEquipmentRButtonUp
end

function BankWindowFix.BankEquipmentRButtonDown( buttonIndex, flags )

	local slot = BankWindow.GetSlotNumberForButtonIndex( buttonIndex )  
	local itemData = BankWindow.GetItem( slot )
    --d("rbuttondown")
    -- verify that we're clicking on an icon before spamming the server
    if not Cursor.IconOnCursor() and DataUtils.IsValidItem( itemData ) then

		
		if( (flags == SystemData.ButtonFlags.SHIFT) and itemData.stackCount > 1 ) then
			-- If Shift is Pressed, Show the stack count window
			ItemStackingWindow.Show(Cursor.SOURCE_BANK, slot)
		else
			 -- Attempt to put this item back into the inventory
            local invitems = DataUtils.GetItems()
            BankWindowFix.moveandstack(itemData, slot, invitems, Cursor.SOURCE_BANK, Cursor.SOURCE_INVENTORY)
			-- RequestMoveItem( Cursor.SOURCE_BANK, slot, Cursor.SOURCE_INVENTORY, GameData.Inventory.FIRST_AVAILABLE_INVENTORY_SLOT, itemData.stackCount)
		end

    end
end

--- RequestMoveItem but attempt to stack items
function BankWindowFix.moveandstack(itemData, slot, invitems, cursorsource, cursortarget)
    if invitems then 
        local tabslot = 1

        if cursortarget == Cursor.SOURCE_BANK then -- to add to current selected tab
            local banktab = BankWindow.currentTabNumber
            tabslot = (banktab - 1) * 80 + 1
            d("tabslot"..tabslot)
        end

        local stackleft = itemData.stackCount
        -- item is stackable
        if itemData.capacity > 1 then
            --d("item is stakcalbe")
            -- find slot wit this item
            local firstgoodslot = 0
            for i, item in ipairs(invitems) do
                if i >= tabslot then
                    if item.uniqueID == itemData.uniqueID and item.capacity > item.stackCount then
                        firstgoodslot = i
                        d("good slot"..tostring(i))
                        local freeslots = math.min(item.capacity - item.stackCount, stackleft)
                        RequestMoveItem( cursorsource, slot, cursortarget, firstgoodslot, freeslots)
                        stackleft = stackleft - freeslots
                        if stackleft < 1 then
                            break
                        end
                    end
                end
            end
        end

        -- item is not stackable or some stacks left
        if stackleft > 0 then
            local firstemptyslot = 0

            for i, item in ipairs(invitems) do
                if i >= tabslot then
                    if item.id == 0 then
                        firstemptyslot = i
                        break
                    end
                end
            end

            if firstemptyslot > 0 then
                RequestMoveItem( cursorsource, slot, cursortarget, firstemptyslot, stackleft)
            end
        end
    end
    --GameData.Inventory.FIRST_AVAILABLE_INVENTORY_SLOT is always 561 whatever that means. 
    --if using this value, calling RequestMoveItem will equip things on character
end

-- OnRButtonUp Handler
function BankWindowFix.BagEquipmentRButtonUp( slot, flags )

    local inventory = EA_Window_Backpack.GetItemsFromBackpack( EA_Window_Backpack.currentMode )
    local itemData = inventory[slot]
    -- If there's no item in this slot, do nothing
    if itemData == nil or itemData.id == nil or itemData.id == 0 then 
        return
    end
    
    local cursorType = EA_Window_Backpack.GetCursorForBackpack( EA_Window_Backpack.currentMode )
    
    local isEnhanceable       = (itemData.numEnhancementSlots > 0)
    local shiftPressed        = (flags == SystemData.ButtonFlags.SHIFT)
    local controlPressed      = (flags == SystemData.ButtonFlags.CONTROL)
    local atStore             = (EA_Window_InteractionStore and EA_Window_InteractionStore.InteractingWithStore ()) or
                                (EA_Window_InteractionLibrarianStore and EA_Window_InteractionLibrarianStore.InteractingWithLibrarianStore ())
    local atRepairMan         = EA_Window_InteractionStore.InteractingWithRepairMan() or EA_Window_InteractionLibrarianStore.InteractingWithRepairMan()
    local isTrading           = EA_Window_Trade.TradeOpen()
    local isMailing           = WindowGetShowing("MailWindow") and WindowGetShowing("MailWindowTabSend")
    local isBankOpen          = BankWindow.IsShowing()
    local isGuildVaultOpen    = GuildVaultWindow.IsVaultOpen()

    local isTradeSkillItem    = DataUtils.IsTradeSkillItem( itemData, nil )
    local isCurrencyItem      = ( itemData.type == GameData.ItemTypes.CURRENCY )
    
    -- Things you can do when using backpack items while not at a store:
    -- 1. Equip the item (if equippable)
    -- 2. Enhance the item, (if enhanceable)
    -- 3. Use the item (left up to the server to determine if it's usable.)
    -- 4. Trade the item (if trade window open)
    -- 5. Mail the item (if the Send Tab of the Mail Window is open)
    -- 6. Put the item in the Guild Vault
    -- 7. Put the item in the crafting window.
    
    -- Block all interactions if the Slot is considered locked
    local slotIsLocked, lockingWindow = EA_Window_Backpack.IsSlotLocked( slot, EA_Window_Backpack.currentMode )
    if slotIsLocked then
        if lockingWindow.windowName == "EA_Window_Trade" then
            EA_Window_Trade.ClearInventoryItem( slot, EA_Window_Backpack.currentMode )

        -- Allow right click to slot stacked crafting items even when they are locked
        elseif ( itemData.stackCount > 1 )
        then
            EA_Window_Backpack.AutoAddCraftingItemIfPossible( slot )
        end
        return
    end

    

    -- If Shift is Pressed on a stacked item, Show the stack count window
    if( shiftPressed and itemData.stackCount > 1 ) then
        ItemStackingWindow.Show( cursorType, slot )
        return
    end
    
    
    if (not atStore and not atRepairMan) then
    
        if isTrading then
            EA_Window_Trade.AddInventoryItem( slot, EA_Window_Backpack.currentMode )
            
        elseif EA_Window_Backpack.AutoAddCraftingItemIfPossible( slot )
        then
            -- The item should be added now, in case all criteria were fulfilled
            
        elseif EquipmentUpgradeWindow and EquipmentUpgradeWindow.AddItem( EA_Window_Backpack.currentMode, slot )
        then
            -- The item should be added now, in case all criteria were fulfilled
            
        elseif EA_Window_Backpack.IsRefinable( itemData ) then
            if( controlPressed )
            then
                EA_Window_Backpack.ConfirmThenRefine( slot, EA_Window_Backpack.currentMode )  
            elseif( isTradeSkillItem or isCurrencyItem )
            then
                TransferBetweenBackpacks( slot, EA_Window_Backpack.currentMode )
            end
            -- right clicking refinable items without holding control should avoid calling SendUseItem

        elseif controlPressed and ENABLE_CTRLRCLICK_DELETE then -- Destroy item
            if DataUtils.IsValidItem( itemData ) then
                local text = GetStringFormat (StringTables.Default.LABEL_TEXT_DESTROY_ITEM_CONFIRM, { itemData.name } )        
                DialogManager.MakeTwoButtonDialog( text, GetString( StringTables.Default.LABEL_YES ), 
                    function () DestroyItem( cursorType, slot ) end, 
                    GetString( StringTables.Default.LABEL_NO ), 
                    nil, nil, nil, nil, nil, nil, DIALOGID_DESTROY_ITEM )
            end

        elseif isBankOpen then
            -- moveandstack (not EA)
            BankWindowFix.moveandstack(itemData, slot, DataUtils.GetBankData (), cursorType, Cursor.SOURCE_BANK)
            --RequestMoveItem( cursorType, slot, Cursor.SOURCE_BANK, GameData.Inventory.FIRST_AVAILABLE_BANK_SLOT, itemData.stackCount )
        
        elseif isGuildVaultOpen then
            GuildVaultWindow.OnRButtonUpBackpack(slot)
        
        elseif isMailing then
            MailWindowTabSend.AttachItem( slot )    -- There is no 'Mailbox' slot.. The item stays in the backpack until the message is sent.
        
        elseif (isEnhanceable and shiftPressed) then
            BeginItemEnhancement (slot)
        
        elseif (itemData.equipSlot > 0) or (itemData.type == GameData.ItemTypes.TROPHY) then
            CharacterWindow.AutoEquipItem( slot )
        
        
        elseif ( isTradeSkillItem or isCurrencyItem )
        then
            TransferBetweenBackpacks( slot, EA_Window_Backpack.currentMode )
        else
            -- try to use the item
            
            -- isHandled returns true if this item requires clicking on another item to be used (e.g. dyes)
            local isHandled = UseItemTargeting.HandleUseItemChangeTargetCursor( cursorType, slot )   
            if not isHandled then
                if not ItemUtils.ShowUseOptions(itemData, GameData.ItemLocs.INVENTORY, slot)
                then
                    SendUseItem( GameData.ItemLocs.INVENTORY, slot, 0, 0, 0 )
                end
                
            end
        end
        return
    end        

    -- Only try selling the item if it has a sellPrice...
    if ( atRepairMan and itemData.broken and itemData.repairPrice > 0 and itemData.repairedName ~= nil and itemData.repairedName ~= L"") then
        -- try to repair item
        if GameData.InteractStoreData.LibrarianType == GameData.InteractStoreData.STORE_TYPE_DEFAULT
        then
            EA_Window_InteractionStore.ConfirmThenRepairItem( slot )  
        else
            EA_Window_InteractionLibrarianStore.ConfirmThenRepairItem( slot )  
        end
        
    elseif atStore and itemData.sellPrice > 0 and not itemData.flags[GameData.Item.EITEMFLAG_NO_SELL] and (not EA_Window_InteractionStore.repairModeOn or not EA_Window_InteractionLibrarianStore.repairModeOn) then
        -- If the player is interacting with a store, try to sell the item.
        if GameData.InteractStoreData.LibrarianType == GameData.InteractStoreData.STORE_TYPE_DEFAULT
        then
            EA_Window_InteractionStore.ConfirmThenSellItem( slot, itemData.stackCount )
        else
            EA_Window_InteractionLibrarianStore.ConfirmThenSellItem( slot, itemData.stackCount )
        end
             
    end
    
end
