# BankWindowFix
- Fixes item stacking when putting items in bank. 
- Makes right-click put thing in backpack. 
- Makes right-click put things into currently open bank tab.