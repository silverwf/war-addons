<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <UiMod name="LargeBackpackFont" version="1.0" date="2019-01-17">
        <VersionSettings gameVersion="1.4.6" windowsVersion="1.0" savedVariablesVersion="1.0"/>

        <Author name="cupnoodles" />
        <Description text="a"/>

        <Dependencies>
            <Dependency name="EA_BackpackWindow"/>
            <!-- <Dependency name="EATemplate_DefaultWindowSkin"/> -->
        </Dependencies>

        <Files>
            <File name="largebackpackfont.lua" />
            <File name="largebackpackfont.xml" />
        </Files>
        
        <SavedVariables>
            <!-- <SavedVariable name="Addon.Settings" /> -->
        </SavedVariables>

        <OnInitialize>
            <!-- <CreateWindow name="Addon" show="true" /> -->
            <CallFunction name="LargeBackpackFont.Initialize" />
        </OnInitialize>

        <OnUpdate>
            <!-- <CallFunction name="addon.Update" /> -->
        </OnUpdate>

        <OnShutdown/>
        
        <WARInfo>
            <Categories>
                <!-- <Category name="CRAFTING" /> -->
            </Categories>
        </WARInfo>

    </UiMod>
</ModuleFile>
